class App {
    static filterer = {}
    static selectedCar = {}
    static container = document.querySelector(".filter")
    static locationContainer = window.pageYOffset + App.container.getBoundingClientRect().top - 70


    constructor() {
        App.locationContainer = window.pageYOffset + App.container.getBoundingClientRect().top - 70
        this.filters = App.filterer
        
        // input type
        this.inputType = document.querySelector(".input.type");
        this.optionType = document.querySelector(".option.type");
        this.selectionType = []
        this.iconType = document.querySelector(".icon-type")
        
        // input date
        this.inputDate = document.querySelector(".input.date")
        this.selectionDate = document.querySelector(".option.date")
        this.iconDate = document.querySelector(".icon-date")
        
        // input time
        this.inputTime = document.querySelector(".input.time")
        this.optionTime = document.querySelector(".option.time")
        this.selectionTime = []
        this.iconTime = document.querySelector(".icon-time")

        // input passenger
        this.inputPassenger = document.querySelector(".input.passenger");
        this.optionPassenger = document.querySelector(".option.passenger");
        this.selectionPassenger = [];
        this.iconPassenger = document.querySelector(".icon-passenger");

        // other
        this.loadButton = document.getElementById("load-btn");
        this.carContainerElement = document.getElementById("cars-container");
    }

    async init() {
      await this.load();

      this.initOptionForm()
      this.initEventListener()
      this.closeAll()
    }


    initOptionForm(){
        // initialize driver type
        const availableTypes = ["Dengan Supir", "Tanpa Supir (Lepas Kunci)"]
        this.initializeOption(this.optionType, availableTypes, '')
        this.selectionType = this.optionType.querySelectorAll("p") 

        // initialize time
        const availableTimes = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"]
        this.initializeOption(this.optionTime, availableTimes, 'WIB') 
        this.selectionTime =  this.optionTime.querySelectorAll("p")

        // initialize passenger
        const availablePassenger = ["0", "1", "2", "3", "4", "5", "6", "7"]
        this.initializeOption(this.optionPassenger, availablePassenger, 'Orang') 
        this.selectionPassenger =  this.optionPassenger.querySelectorAll("p")
    }


    initEventListener(){
        // input type
        this.inputType.onclick = () => this.openOption(this.inputType, this.optionType, this.iconType, 'icon-type')
        this.selectionType.forEach(p => p.onclick = () => this.closeOption('type', p.dataset.value))

        // input date
        this.inputDate.onclick = () => this.openOptionDate()
        this.selectionDate.onchange = () => this.closeOption('date', this.selectionDate.value.split("-").reverse().join("-"))

        // input time
        this.inputTime.onclick = () => this.openOption(this.inputTime, this.optionTime, this.iconTime, "icon-time")
        this.selectionTime.forEach(p => p.onclick = () => this.closeOption('time', p.dataset.value))

        // input passenger
        this.inputPassenger.onclick = () => this.openOption(this.inputPassenger, this.optionPassenger, this.iconPassenger, 'icon-passenger')
        this.selectionPassenger.forEach(p => p.onclick = () => this.closeOption('passenger', +p.dataset.value))

        // search button
        this.loadButton.onclick = (e) => this.redirect(e);

        // remove shadow if user click outside div
        const forms = Array.from(document.querySelectorAll(".form"))
        window.addEventListener('click', e=> {
            if (!forms.some(form => form.contains(e.target))) {
                this.closeAll()
                App.container.classList.remove("full-shadow")
            }
            else{
                App.container.classList.add("full-shadow")
                window.scrollTo(0, App.locationContainer)
            }
        }, true)

        window.addEventListener("resize", () => {
            App.locationContainer = window.pageYOffset + App.container.getBoundingClientRect().top - 70
        })
    }

    initializeOption(target, values, extension){
        values.forEach(value => {
            const newHTML =  `
                              <p class="d-flex justify-content-between" data-value='${value}'>
                                  <span>${value}</span>
                                  <span>${extension}</span>
                              </p>
                              `
            target.insertAdjacentHTML("beforeend", newHTML)
        })
    }


    closeAll(){
        let inputs = [this.inputType, this.inputDate, this.inputTime, this.inputPassenger]
        let options = [this.optionType, this.optionTime, this.optionPassenger]
        inputs.forEach(input => input.classList.remove("selected"))
        options.forEach(option => option.classList.remove("show"))

        this.inputType.value = this.filters.type
        this.inputDate.value = this.filters.date
        this.inputTime.value = this.filters.time ? `${this.filters.time} WIB` : ''
        this.inputPassenger.value = this.filters.passenger >= 0 ? `${this.filters.passenger} orang` : ''

        this.iconType.classList = 'fa-solid fa-angle-down icon-form icon-type'
        this.iconDate.classList = 'fa-regular fa-calendar icon-form icon-date'
        this.iconTime.classList = "fa-regular fa-clock icon-form icon-time"
        this.iconPassenger.classList = "fa-regular fa-user icon-form icon-passenger"
    }

    openOptionDate(){
        const open = () => {
            this.closeAll()
            this.inputDate.classList.add("selected")
            this.iconDate.classList = 'fa-solid fa-angle-down icon-form icon-date'
            this.selectionDate.showPicker()
        }

        if (Math.floor(window.pageYOffset) != Math.floor(App.locationContainer)){
            setTimeout(() => {
                open()
            }, 250)  
        }
        else{
            open()
        }
        
    }

    openOption(input, option, icon, iconName){
        const open = () => {
            this.closeAll()
            icon.classList = `fa-solid fa-angle-down icon-form ${iconName}`
            input.classList.add("selected")
            option.classList.add("show")
        }
        if (Math.floor(window.pageYOffset) != Math.floor(App.locationContainer)){
            setTimeout(() => {
                open()
            }, 250)
        }
        else{
            open()
        }
    }

    closeOption(key, value){
        console.log("closed")
        this.filters[key] = value
        App.container.classList.remove("full-shadow")
        this.closeAll()
    }

    redirect(e){
        if (!this.filters.type){
            alert("Please choose type")
            e.preventDefault()
            return
        }
        if (!this.filters.date){
            alert("Please choose date")
            e.preventDefault()
            return
        }
        if (!this.filters.time){
            alert("Please choose time")
            e.preventDefault()
            return
        }
        window.location.href = window.location.pathname + `?type=${this.filters.type}&date=${this.filters.date}&time=${this.filters.time}&passenger=${this.filters.passenger}`
    }

    static openPopup(id){
        const car = Car.list.filter(car => car.id === id)[0]
        const popup = document.querySelector(".popup")
        const popupText = document.querySelector(".popup-text")

        const selectCar = (selectedCar) => {
            App.selectedCar = JSON.parse(JSON.stringify(selectedCar))
            popup.classList.add("show")
            setTimeout(() => {
                popup.classList.remove("show")
            }, 2500) 
            popupText.textContent = `Successfully booked car with id = ${App.selectedCar.id}`
        }

        if (Object.keys(App.selectedCar).length === 0){
            selectCar(car)
        } else{
            if (window.confirm("You want to delete your last selected car ? ")){
                selectCar(car)
            }
        }
    }



    run = () => {
        Car.list.forEach((car) => {
            const node = document.createElement("div");
            node.className = "card"
            node.innerHTML = car.render();
            this.carContainerElement.appendChild(node);
        });
    };

    filtering(car){ 
        car = (
              car.transmission === App.filterer.transmission
              && new Date(car.availableAt).getTime() >= new Date(`${App.filterer.date.split("-").reverse().join("-")} ${App.filterer.time}`).getTime()
              ) ? car : false

        if (!car) return false
         
        if (App.filterer.passenger){
            car = (car.capacity >= App.filterer.passenger) ? car : false
        }
        
        return car
    }

    async load() {
        const cars = await Binar.listCars(this.filtering);
        Car.init(cars);
    }

    clear = () => {
        let child = this.carContainerElement.firstElementChild;

        while (child) {
          child.remove();
          child = this.carContainerElement.firstElementChild;
        }
    };
}
