/*
 * Contoh kode untuk membaca query parameter,
 * Siapa tau relevan! :)
 * */

const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());

// Coba olah data ini hehe :)
console.log(params);

App.filterer = (Object.keys(params).length === 0) ? { type: '', date: '', time: '', passenger: -1 }: {...params, transmission: "Manual"}
console.log(App.filterer)
/*
 * Contoh penggunaan DOM di dalam class
 * */
const app = new App();

app.init().then(app.run);
